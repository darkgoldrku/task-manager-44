package ru.t1.bugakov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IAbstractUserOwnedDTORepository<TaskDTO> {
    @Nullable List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

}
