package ru.t1.bugakov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.service.IAuthService;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.dto.ISessionDTOService;
import ru.t1.bugakov.tm.api.service.dto.IUserDTOService;
import ru.t1.bugakov.tm.dto.model.SessionDTO;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.field.LoginEmptyException;
import ru.t1.bugakov.tm.exception.field.PasswordEmptyException;
import ru.t1.bugakov.tm.exception.user.AccessDeniedException;
import ru.t1.bugakov.tm.exception.user.PermissionException;
import ru.t1.bugakov.tm.util.CryptUtil;
import ru.t1.bugakov.tm.util.HashUtil;

import java.sql.SQLException;
import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserDTOService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionDTOService sessionService;

    public AuthService(@NotNull final IUserDTOService userService, @NotNull IPropertyService propertyService, @NotNull ISessionDTOService sessionService) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public UserDTO registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException {
        userService.create(login, password, email);
        return userService.findByLogin(login);
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = userService.findByLogin(login);
        if (user.isLocked()) throw new PermissionException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new PasswordEmptyException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) throws SQLException {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        sessionService.add(session);
        return session;
    }

    @Override
    @NotNull
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getCreated();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(@Nullable final SessionDTO session) throws SQLException {
        if (session == null) return;
        sessionService.remove(session);
    }

}
