package ru.t1.bugakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.model.ITaskRepository;
import ru.t1.bugakov.tm.model.Task;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public @Nullable List<Task> findAll(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();

    }

    @Override
    public @Nullable List<Task> findAllWithSort(@NotNull String userId, @Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        @NotNull final Root<Task> root = criteriaQuery.from(Task.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public Task findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);

    }

    @Override
    public int getSize(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();

    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM Task m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public @Nullable List<Task> findAll() {
        @NotNull final String jpql = "SELECT m FROM Task m";
        return entityManager.createQuery(jpql, Task.class).getResultList();

    }

    @Override
    public @Nullable List<Task> findAllWithSort(@Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        @NotNull final Root<Task> from = criteriaQuery.from(Task.class);
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public @Nullable Task findById(@NotNull String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

}


