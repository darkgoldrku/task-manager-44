package ru.t1.bugakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.model.IUserRepository;
import ru.t1.bugakov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM User m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Nullable
    @Override
    public List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class).getResultList();
    }

    @Nullable
    @Override
    public List<User> findAllWithSort(@Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        @NotNull final Root<User> from = criteriaQuery.from(User.class);
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public User findById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM User m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}


