package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskRemoveByIdRequest(@Nullable String id) {
        this.id = id;
    }

    public TaskRemoveByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
