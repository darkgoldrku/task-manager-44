package ru.t1.bugakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectRemoveByIdRequest(@Nullable String id) {
        this.id = id;
    }

    public ProjectRemoveByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
